<?php
// session_start();
include_once 'session.php';
include_once 'database.php';

$db = db_connect();
$student_user_id = $_SESSION['u_id'];
$current_date = date("Y-m-d");
$student_dept_sql = "SELECT dept_id FROM student WHERE u_id = '$student_user_id' ";
$student_dept_query = mysqli_query($db,$student_dept_sql);
$student_dept_result = mysqli_fetch_assoc($student_dept_query);
$final_dept_id = $student_dept_result['dept_id'];
$select_notice_sql = "SELECT notice,end_date FROM course_reg_time WHERE dept_id = '$final_dept_id' ";
$select_notice_query = mysqli_query($db,$select_notice_sql);
$select_notice_result = mysqli_fetch_assoc($select_notice_query);

// print_r($select_notice_result['notice']);
// exit();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pre Registration</title>
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="assets/img/favicon.ico" type="image/x-icon">
    <link href="assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/font-awesome/font-awesome.min.css" rel="stylesheet" integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg==" crossorigin="anonymous">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
      <!--Header Start---------->
      <header class="header">
          <div class="container">
              <div class="row">
                  <div class="col-md-8">
                      <div class="logo"><a href="index.php"><img src="assets/img/logo.png"></a></div>
                      <div class="headline"><h3>STATE UNIVERSITY OF BANGLADESH</h3></div>
                  </div>
                  <div class="col-md-4">
                      <div class="phone">
                          <i class="fa fa-phone"></i> +900 6584 4858
                      </div>
                  </div>
              </div>
          </div>
      </header>
      <div class="container">
        <div class="col-md-12">
          <div class="alert alert-danger" role="alert">
            <!-- <button type="button" class="close" data-dismiss="alert"><span class="sr-only">Close</span></button> -->
            <strong><marquee>
              <?php
              if ($select_notice_result['end_date'] >= $current_date) {
                echo $select_notice_result['notice'];
              } else {
                echo "Pre-Registration Date Over!!";
            }
            ?>
            </marquee></strong>
          </div>
        </div>
      </div>
                             <!--Header End------------>
                             <!---Navigation Menu Start------------>
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #E8E8F1;">
                      <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link" href="student_dashboard.php"><strong>Profile</strong></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="registered_course.php"><strong>Preregistered Courses</strong></a>
                        </li>
                          <li class="nav-item">
                            <a class="nav-link" href="offered_course.php"><strong>Offered Courses</strong></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="syllabus.php"><strong>Syllabus</strong></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="logout.php"><strong>Logout</strong></a>
                          </li>
                      </ul>
                  </nav>
              </div>
          </div>
      </div>
      <!--Navigation Menu Ends------------>
