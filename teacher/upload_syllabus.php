<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
$teacher_id = $_SESSION['u_id'];
if ($db) {
  $teacher_dept_sql = "SELECT dept_id from teacher where u_id = '$teacher_id' ";
  $teacher_dept_query = mysqli_query($db,$teacher_dept_sql);
  if (mysqli_num_rows($teacher_dept_query) > 0 ) {
     $teacher_dept_result = mysqli_fetch_assoc($teacher_dept_query);
     $teacher_dept_id = $teacher_dept_result['dept_id'];
     // var_dump($teacher_dept_id);
     // exit();
     }
   }
?>
<div class="container">
    <div class="row">
        <!-- <div class="col-md-3">
        </div> -->
        <div class="col-md-12">
          <?php
          if (isset($_GET['sm']) && $_GET['sm'] == "success") {
            echo "<div class='alert alert-success mt-2'><strong>FILE UPLOADED SUCCESSFULLY!</strong></div>";
          }
          ?>
            <form method="post" action="excel_upload.php" enctype="multipart/form-data">
              <div class="form-group mt-5">
                      <div class="form-group col-md-4">
                        <label>Syllabus</label>
                          <select name="syllabus" class="form-control" id="exampleFormControlSelect1" required >
                              <option value="">Select Syllabus Version</option>
                              <?php
                              if ($db) {
                                  $syllabus_sql = "SELECT * FROM syllabus ";
                                  $syllabus_query = mysqli_query($db,$syllabus_sql);
                                }
                              if (mysqli_num_rows($syllabus_query) > 0){
                                  while ( $syllabus_result = mysqli_fetch_assoc($syllabus_query)){
                              ?>
                                  <option value="<?php echo $syllabus_result['id']; ?>"> <?php echo $syllabus_result['version'];?> </option>
                              <?php
                                  } }
                              ?>
                          </select>
                      </div>
                </div>
              <div class="form-group ml-3">
                <label for="exampleFormControlFile1">Excel file input</label>
                <input type="file" name="file" class="form-control-file" id="exampleFormControlFile1">
              </div>
              <div class="form-group ml-3">
                <input type="hidden" name="dept_id" value="<?php echo $teacher_dept_id; ?>">
                <input class="btn btn-outline-primary" type="submit" value="Submit">
              </div>

            </form>
        </div>
        <!-- <div class="col-md-3">
        </div> -->
    </div>
</div>
<?php include_once 'footer.php' ;?>
