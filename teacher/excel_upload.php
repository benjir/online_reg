<?php
include_once '../database.php';
require '../vendor/autoload.php';

$db = db_connect();

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Reader\Csv;
use PhpOffice\PhpSpreadsheet\Reader\Xlsx;

$file_mimes = array('text/x-comma-separated-values', 'text/comma-separated-values', 'application/octet-stream', 'application/vnd.ms-excel', 'application/x-csv', 'text/x-csv', 'text/csv', 'application/csv', 'application/excel', 'application/vnd.msexcel', 'text/plain', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');

if(isset($_FILES['file']['name']) && isset($_POST['dept_id']) && isset($_POST['syllabus']) && in_array($_FILES['file']['type'], $file_mimes)) {
    $teacher_dept_id = $_POST['dept_id'];
    $syllabus_id = $_POST['syllabus'];
    $arr_file = explode('.', $_FILES['file']['name']);
    $extension = end($arr_file);

    if('csv' == $extension) {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Csv();
    } else {
        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
    }

    $spreadsheet = $reader->load($_FILES['file']['tmp_name']);

    $sheetData = $spreadsheet->getActiveSheet()->toArray();
    echo "<pre>";
    print_r($sheetData);
    exit();
    $dept_id = $teacher_dept_id;
    foreach($sheetData as $row ){
      $course_name = $row[0];
      $course_code = $row[1];
      $course_credit = $row[2];
      $pre_course = $row[3];
      $course_insert_sql = "INSERT INTO course (code,name,credit,pre_course,dept_id,syllabus_id) VALUES ('$course_name', '$course_code','$course_credit','$pre_course','$dept_id','$syllabus_id') ";
      // echo "$course_insert_sql";
      // exit();
      $course_insert_query = mysqli_query($db,$course_insert_sql);
      if ($course_insert_query) {
        header("Location: upload_syllabus.php?sm=success");
      } else {
        echo "There is a problem uploading the file".mysqli_connect_error();
      }

    }

}
?>
