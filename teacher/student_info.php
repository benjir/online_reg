<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();

$student_id = $_GET['sid'];
if ($db) {
  $student_dept_sql = "SELECT dept_id from student where u_id = '$student_id' ";
  $student_dept_query = mysqli_query($db,$student_dept_sql);
  if (mysqli_num_rows($student_dept_query) > 0 ) {
     $student_dept_result = mysqli_fetch_assoc($student_dept_query);
     $student_dept_id = $student_dept_result['dept_id'];
   }
 }

$sn = 1;
$student_details_sql = "SELECT * FROM student WHERE u_id = '$student_id' ";
$student_details_query = mysqli_query($db,$student_details_sql);
$student_details_result = mysqli_fetch_assoc($student_details_query);
$student_details_semester = $student_details_result['semester'];
$student_dept_id = $student_details_result['dept_id'];
$student_dept_sql = "SELECT * FROM dept WHERE id = '$student_dept_id' ";
$student_dept_query = mysqli_query($db,$student_dept_sql);
$student_dept_result = mysqli_fetch_assoc($student_dept_query);
$student_prog_id = $student_details_result['prog_id'];
$student_prog_sql = "SELECT * FROM program WHERE id = '$student_prog_id' ";
$student_prog_query = mysqli_query($db,$student_prog_sql);
$student_prog_result = mysqli_fetch_assoc($student_prog_query);
$student_session_id = $student_details_result['session_id'];
$student_session_sql = "SELECT * FROM session WHERE id = '$student_session_id' ";
$student_session_query = mysqli_query($db,$student_session_sql);
$student_session_result = mysqli_fetch_assoc($student_session_query);
?>
<div id="div_print">
<div class="container">
    <div class="row">
      <div class="col-md-12"><h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Student Details</h5></div>
        <div class="col-md-12 mt-3">
          <?php
          if (isset($_GET['sm']) && $_GET['sm'] == "success") {
            echo "<div class='alert alert-success text-center'><strong>Profile Edited Successfully !!</strong></div>";
          }
          ?>
        </div>
          <div class="col-md-4 offset-2">
              <p>ID Number: <?php echo $student_details_result['u_id'];?></p>
              <p>Name of Student: <?php echo $student_details_result['name'];?></p>
              <p>Name of Guardian: <?php echo $student_details_result['guardian_name'];?></p>
              <p>Mailing Address : <?php echo $student_details_result['current_address'];?></p>
              <p>Permanent Address : <?php echo $student_details_result['permanent_address'];?></p>
              <p>Contact No : <?php echo $student_details_result['phone'];?></p>
          </div>
          <div class="col-md-6">
              <p>Email : <?php echo $student_details_result['email'];?></p>
              <p>Relationship With Guardian : <?php echo $student_details_result['guardian_rel'];?></p>
              <p>Name of Dept : <?php echo $student_dept_result['name'];?></p>
              <p>Program : <?php echo $student_prog_result['name'];?></p>
              <p>Semester : <?php echo $student_details_semester;?></p>
              <p>Year : <?php echo $student_session_result['year'];?></p>
          </div>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
              <!--  Current Semester Course Table Ends -->
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Registered Courses</h5>
            <?php
            if (isset($_GET['dm']) && $_GET['dm'] == "delete") {
              echo "<div class='alert alert-danger text-center'><strong>Course Deleted!!</strong></div>";
            }
            ?>
            <!--  Registered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Credit</th>
                          <th scope="col">Course Type</th>
                          <th scope="col">Course Status</th>
                          <!-- <th scope="col">Action</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $student_pending_course_sql = "SELECT course_id,type_id from course_registration where u_id = '$student_id' AND (status = 'PENDING' OR status = 'REGISTERED') ";
                        $student_pending_course_query = mysqli_query($db,$student_pending_course_sql);
                        if (mysqli_num_rows($student_pending_course_query) > 0 ) {
                           while($student_pending_course_result = mysqli_fetch_assoc($student_pending_course_query)){
                            $student_pending_course_id = $student_pending_course_result['course_id'];
                            $student_pending_course_type_id = $student_pending_course_result['type_id'];
                            $course_details_sql = "SELECT * from course where id = '$student_pending_course_id' AND dept_id = '$student_dept_id' ";
                            $course_details_query = mysqli_query($db,$course_details_sql);
                            if (mysqli_num_rows($course_details_query) > 0 ) {
                                while($course_details_query_result = mysqli_fetch_assoc($course_details_query)){
                                  ?>
                                <tr>
                                  <th scope="row"><?php echo $sn++;?></th>
                                  <td><?php echo $course_details_query_result['name'];?></td>
                                  <td><?php echo $course_details_query_result['code'];?></td>
                                  <td><?php echo $course_details_query_result['credit'];?></td>
                                  <td>
                                    <?php
                                    $course_type_name_sql = "SELECT type from type where id = '$student_pending_course_type_id' ";
                                    $course_type_name_query = mysqli_query($db,$course_type_name_sql);
                                    if (mysqli_num_rows($course_type_name_query) > 0 ) {
                                       $course_type_name_result = mysqli_fetch_assoc($course_type_name_query);
                                       echo $course_type_name_result['type'];
                                     }
                                    ?>
                                  </td>
                                  <td>
                                    <?php
                                    $course_status_sql = "SELECT status from course_registration where course_id = '$student_pending_course_id' AND u_id='$student_id'";
                                    $course_status_query = mysqli_query($db,$course_status_sql);
                                    if (mysqli_num_rows($course_status_query) > 0 ) {
                                       $course_status_result = mysqli_fetch_assoc($course_status_query);
                                       echo $course_status_result['status'];
                                     }
                                    ?>
                                  </td>
                                  <!-- <td>
                                    <a class="btn btn-outline-primary "href="delete_added_course.php?id=<?php //echo $student_pending_course_id;?>" onclick="return confirm('Are u sure want to delete this?')">Delete</a>
                                  </td> -->
                                </tr>
                                <?php }
                              }
                            }
                          } else {
                            echo '<p class="text-center bg-danger"> No Course Added </p>';
                          }
                          ?>
                      </tbody>
                  </table>
              </div>
              <!--  Registered Course Table Ends -->
        </div>
    </div>
</div>
</div>
<div class="container">
  <div class="row mt-3">
      <div class="col-md-5">
      </div>
      <div class="col-md-2">
          <div class="form-group row">
              <input class="btn btn-outline-primary btn-block" type="button" onClick="printDiv('div_print');" value="Print">
          </div>
          <!-- <div class="form-group row">
              <input class="btn btn-primary" type="button" value="Download">
          </div> -->
      </div>
     <!--  <div class="col-md-1">
        <div class="form-group">
              <input class="btn btn-outline-primary" type="button" value="Download">
        </div>
      </div> -->
      <div class="col-md-5">
      </div>
  </div>
</div>
<?php include_once 'footer.php';?>
