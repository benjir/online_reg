<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
$user_id=$_SESSION['u_id'];
$user_dept_sql = "SELECT * FROM teacher ";
$user_dept_query = mysqli_query($db,$user_dept_sql);
$user_dept_result = mysqli_fetch_assoc($user_dept_query);
$teacher_dept_id = $user_dept_result['dept_id'];
?>
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <h5 class="text-center p-3 mb-5 mt-2 bg-secondary text-white">Course Pre-Requisite</h5>
      </div>
      <div class="col-md-3">
      </div>
      <div class="col-md-6 mt-5">
        <?php
        if (isset($_GET['sm']) && $_GET['sm'] == "success") {
          echo "<div class='alert alert-success mt-2 text-center'><strong>Pre-Requisite Course Entry successful! </strong></div>";
        }
        ?>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Select Course</label>
            <div class="col-sm-8">
              <form class="" action="course_pre_req_p.php" method="post">
                <div class="form-group">
                    <select name="course" class="form-control" id="exampleFormControlSelect1" required >
                        <option>Select Course</option>
                        <?php
                        if ($db) {
                            $course_sql = "SELECT * FROM course WHERE dept_id='$teacher_dept_id' ";
                            $course_query = mysqli_query($db,$course_sql);
                        }
                        if (mysqli_num_rows($course_query) > 0) {
                            while ( $course_result = mysqli_fetch_assoc($course_query)){
                            ?>
                            <option value="<?php echo $course_result['id']; ?>"> <?php echo $course_result['name'];?></option>
                        <?php
                            } }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Select Pre Course</label>
            <div class="col-sm-8">
                <div class="form-group">
                    <select name="pre_course" class="form-control" id="exampleFormControlSelect1" required >
                        <option>Select Course</option>
                        <?php
                        if ($db) {
                            $course_sql = "SELECT * FROM course WHERE dept_id='$teacher_dept_id' ";
                            $course_query = mysqli_query($db,$course_sql);
                        }
                        if (mysqli_num_rows($course_query) > 0) {
                            while ( $course_result = mysqli_fetch_assoc($course_query)){
                            ?>
                            <option value="<?php echo $course_result['id']; ?>"> <?php echo $course_result['name'];?></option>
                        <?php
                            } }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-md-5">

          </div>
          <div class="col-md-3">
            <div class="form-group">
               <input type="hidden" name="dept_id" value="<?php echo $teacher_dept_id; ?>">
               <input type="submit" name="submit" class="btn btn-outline-primary btn-lg btn-block" value="Submit">
            </div>
          </div>
        </form>
          <div class="col-md-4">
          </div>
        </div>
      </form>
    </div>
</div>
<?php include_once 'footer.php';?>
