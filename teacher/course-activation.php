<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
$u_id = $_SESSION['u_id'];
$teacher_details = array();
if ($db) {
  $teacher_dept_sql = "SELECT * from teacher where u_id = '$u_id' ";
  // echo $teacher_dept_sql; exit();
  $teacher_dept_query = mysqli_query($db,$teacher_dept_sql);
  if (mysqli_num_rows($teacher_dept_query) > 0 ) {
     $teacher_details = mysqli_fetch_assoc($teacher_dept_query);
   }
 }
$sn = 1;
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">

    </div>

  </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Student Course Activation</h5>
            <!--  Offered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Student ID</th>
                          <th scope="col">Student Name</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($db) {
                          $pending_reg_student_sql = "SELECT s.u_id student_id, s.name student_name FROM course_registration cr JOIN course c ON cr.course_id=c.id JOIN student s ON s.u_id=cr.u_id WHERE cr.teacher_id=$teacher_details[id] AND (cr.status='PENDING' OR cr.status='REGISTERED') GROUP BY student_id";
                          // echo $pending_reg_student_sql."<br>";
                          $pending_reg_student_query = mysqli_query($db,$pending_reg_student_sql);
                          if (mysqli_num_rows($pending_reg_student_query) > 0 ) {
                            while ($pending_reg_student_query_result = mysqli_fetch_assoc($pending_reg_student_query)) {
                        ?>
                              <tr>
                                <th scope="row"><?php echo $sn++; ?></th>
                                <td><?php echo $pending_reg_student_query_result['student_id']; ?></td>
                                <td><?php echo $pending_reg_student_query_result['student_name']; ?></td>
                                <td>
                                  <a class="btn btn-outline-primary" href="student_registration_details.php?sid=<?php echo $pending_reg_student_query_result['student_id']; ?>">Course Details</a>
                                  <a class="btn btn-outline-primary" href="student_info.php?sid=<?php echo $pending_reg_student_query_result['student_id']; ?>">Student Details</a>
                                </td>
                              </tr>
                      <?php
                            }
                          }
                        }
                      ?>
                      </tbody>
                  </table>
              </div>
              <!--  Offered Course Table Ends -->
        </div>
    </div>
</div>
<?php include_once 'footer.php';?>
