<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
$user_id = $_SESSION['u_id'];
$teacher_details_sql = "SELECT * FROM teacher WHERE u_id = '$user_id' ";
$teacher_details_query = mysqli_query($db,$teacher_details_sql);
$teacher_details_result = mysqli_fetch_assoc($teacher_details_query);

$teacher_dept_id = $teacher_details_result['dept_id'];
$teacher_dept_sql = "SELECT * FROM dept WHERE id = '$teacher_dept_id' ";
$teacher_dept_query = mysqli_query($db,$teacher_dept_sql);
$teacher_dept_result = mysqli_fetch_assoc($teacher_dept_query);
// $student_prog_id = $student_details_result['prog_id'];
// $student_prog_sql = "SELECT * FROM program WHERE id = '$student_prog_id' ";
// $student_prog_query = mysqli_query($db,$student_prog_sql);
// $student_prog_result = mysqli_fetch_assoc($student_prog_query);
// $student_session_id = $student_details_result['session_id'];
// $student_session_sql = "SELECT * FROM session WHERE id = '$student_session_id' ";
// $student_session_query = mysqli_query($db,$student_session_sql);
// $student_session_result = mysqli_fetch_assoc($student_session_query);
?>
<div class="container">
    <div class="row">
        <div class="col-md-7 offset-5 mt-5">
        	<p class="font-weight-bold">Name : <?php echo $teacher_details_result['name'];?></p>
        	<p class="font-weight-bold">USER ID : <?php echo $teacher_details_result['u_id'];?></p>
        	<p class="font-weight-bold">Dept : <?php echo $teacher_dept_result['name'];?></p>
        	<p class="font-weight-bold">Phone : <?php echo $teacher_details_result['phone'];?></p>
        	<p class="font-weight-bold">Email : <?php echo $teacher_details_result['email'];?></p>
        	<!-- <p>Dept : CSE</p> -->
        </div>
        <!-- <div class="col-md-3">
        </div> -->
    </div>
</div>
<?php include_once 'footer.php';?>
