    <div class="container">
        <div class="row">
            <div class="col-md-12 footer_margin footer_color">
                <p class="text-center p-3 mb-2 bg-secondary text-white">COPYRIGHT © <?php echo date("Y"); ?>, STATE UNIVERSITY OF BANGLADESH. ALL RIGHTS RESERVED.</p>
            </div>
        </div>
    </div>
                                                <!------------Footer End------------>
<script src="assets/js/jquery-3.2.1.min.js"></script>
<script src="assets/js/popper.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script language="javascript">

function printDiv(divName) {
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;

     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
}
</script>
</body>
</html>
