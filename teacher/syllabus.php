<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
$teacher_id = $_SESSION['u_id'];
//var_dump($user_id);
if ($db) {
  $teacher_dept_sql = "SELECT dept_id from teacher where u_id = '$teacher_id' ";
  $teacher_dept_query = mysqli_query($db,$teacher_dept_sql);
  if (mysqli_num_rows($teacher_dept_query) > 0 ) {
     while ($teacher_dept_result = mysqli_fetch_assoc($teacher_dept_query)){
       $teacher_dept_id = $teacher_dept_result['dept_id'];
     }
   }
 }
$sn = 1;
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">

    </div>

  </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Course List</h5>
            <?php
            if (isset($_GET['sm']) && $_GET['sm'] == "success") {
              echo "<div class='alert alert-success text-center'><strong>Course Offered For This Semester!!</strong></div>";
            }
            ?>
            <!--  Offered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Course Credit</th>
                          <th scope="col">Pre-requisite</th>
                          <th scope="col">Syllabus Version</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($db) {
                          $course_sql = "SELECT * from course where dept_id = '$teacher_dept_id' ";
                          $course_query = mysqli_query($db,$course_sql);
                        }
                           if (mysqli_num_rows($course_query) > 0 ) {
                              while ($course_result = mysqli_fetch_assoc($course_query)){
                                $course_id = $course_result['id'];
                                $course_offered_check = $course_result['is_offered'];
                                $syllabus_id = $course_result['syllabus_id'];
                              // echo '<pre>';
                              // echo '</pre>';
                              // print_r($course_result);
                        ?>
                        <tr>
                          <th scope="row"><?php echo $sn ++;?></th>
                          <td><?php echo $course_result['name'];?></td>
                          <td><?php echo $course_result['code'];?></td>
                          <td><?php echo $course_result['credit'];?></td>
                          <td><?php echo $course_result['pre_course'];?></td>
                          <td>
                            <?php
                              $syllabus_sql = "SELECT * from syllabus where id = '$syllabus_id' ";
                              $syllabus_query = mysqli_query($db,$syllabus_sql);
                              $syllabus_result = mysqli_fetch_assoc($syllabus_query);
                              echo $syllabus_result['version'];
                            ?>
                          </td>
                          <td>
                            <form class="" action="offered_course_p.php" method="post">
                              <div class="form-group">
                                  <input type="hidden" name="course_id" value="<?php echo $course_id; ?>">
                                  <input type="hidden" name="dept_id" value="<?php echo $teacher_dept_id; ?>">
                                  <?php 
                                  if($course_offered_check == 'YES'){?>
                                    <input type="button" class="btn btn-outline-primary disabled" value="Offered">
                                  <?php } else{ ?>
                                  <input type="submit" name="register" class="btn btn-outline-primary" value="Offer">
                                <?php }?>
                                  <!-- <input type="submit" name="register" class="btn btn-outline-primary" value="Withdraw"> -->
                              </div>
                            </form>
                          </td>
                        </tr>
                      <?php } }  ?>
                      </tbody>
                  </table>
              </div>
              <!--  Offered Course Table Ends -->
        </div>
    </div>
</div>
<?php include_once 'footer.php';?>
