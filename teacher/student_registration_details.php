<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 't_header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();
// $student_id = $_SESSION['u_id'];
if (isset($_GET['sid'])) {
  $student_id = $_GET['sid'];
  $student_dept_sql = "SELECT dept_id from student where u_id = '$student_id' ";
  $student_dept_query = mysqli_query($db,$student_dept_sql);
  $student_dept_result = mysqli_fetch_assoc($student_dept_query);
  $student_dept_id = $student_dept_result['dept_id'];
  // print_r($student_dept_id);
  // exit();
 }

$sn = 1;
?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
              <!--  Current Semester Course Table Ends -->
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Registered Course List</h5>
            <?php
            if (isset($_GET['dm']) && $_GET['dm'] == "delete") {
              echo "<div class='alert alert-danger text-center'><strong>Course Deleted!!</strong></div>";
            }
            if (isset($_GET['ap']) && $_GET['ap'] == "approve") {
              echo "<div class='alert alert-danger text-center'><strong>Course Approved!!</strong></div>";
            }
            if (isset($_GET['rj']) && $_GET['rj'] == "rejected") {
              echo "<div class='alert alert-danger text-center'><strong>Course Rejected!!</strong></div>";
            }
            ?>
            <!--  Registered Course Table Start ap=approve -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Credit</th>
                          <th scope="col">Course Type</th>
                          <th scope="col">Course Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $student_pending_course_sql = "SELECT course_id,type_id,status from course_registration where (status='PENDING' OR status='REGISTERED' OR status='REJECTED') AND u_id = '$student_id'";
                        $student_pending_course_query = mysqli_query($db,$student_pending_course_sql);
                        if (mysqli_num_rows($student_pending_course_query) > 0 ) {
                           while($student_pending_course_result = mysqli_fetch_assoc($student_pending_course_query)){
                            $student_pending_course_id = $student_pending_course_result['course_id'];
                            $student_pending_course_type_id = $student_pending_course_result['type_id'];
                            $course_details_sql = "SELECT * from course where id = '$student_pending_course_id' AND dept_id = '$student_dept_id' ";
                            $course_details_query = mysqli_query($db,$course_details_sql);
                            if (mysqli_num_rows($course_details_query) > 0 ) {
                                while($course_details_query_result = mysqli_fetch_assoc($course_details_query)){
                                  ?>
                                <tr>
                                  <th scope="row"><?php echo $sn++;?></th>
                                  <td><?php echo $course_details_query_result['name'];?></td>
                                  <td><?php echo $course_details_query_result['code'];?></td>
                                  <td><?php echo $course_details_query_result['credit'];?></td>
                                  <td>
                                    <?php
                                    $course_type_name_sql = "SELECT type from type where id = '$student_pending_course_type_id' ";
                                    $course_type_name_query = mysqli_query($db,$course_type_name_sql);
                                    if (mysqli_num_rows($course_type_name_query) > 0 ) {
                                       $course_type_name_result = mysqli_fetch_assoc($course_type_name_query);
                                       echo $course_type_name_result['type'];
                                     }
                                    ?>
                                  </td>
                                  <td><?php echo $student_pending_course_result['status'];?></td>
                                  <!-- <form class="" action="approve_student_course.php" method="post"> -->
                                    <td>
                                      <a class="btn btn-outline-primary" href="approve_student_course.php?id=<?php echo $student_pending_course_result['course_id']; ?>&sid=<?php echo $student_id;?>">Approve</a>
                                      <a class="btn btn-outline-primary ml-2" href="reject_student_course.php?id=<?php echo $student_pending_course_result['course_id']; ?>&sid=<?php echo $student_id;?>">Reject</a>
                                    </td>
                                  <!-- </form> -->
                                </tr>
                                <?php }
                              }
                            }
                          } else {
                            echo '<p class="text-center bg-danger"> All Courses Approved </p>';
                          }
                          ?>
                      </tbody>
                  </table>
              </div>
              <!--  Registered Course Table Ends -->
        </div>
    </div>
</div>
<?php include_once 'footer.php';?>
