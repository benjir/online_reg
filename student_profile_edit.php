<?php
include_once 'database.php';
include_once 'session.php';
include_once 's_header.php';
$db = db_connect();
if (isset($_GET['id'])) {
$user_id = $_GET['id'];
$user_pass_sql = "SELECT password FROM user WHERE u_id = '$user_id' ";
$user_pass_query = mysqli_query($db,$user_pass_sql);
$user_pass_result = mysqli_fetch_assoc($user_pass_query);
}
if (isset($_GET['id'])) {
  $student_id = $_GET['id'];
  $student_details_sql = "SELECT * FROM student WHERE u_id = '$student_id' ";
  $student_details_query = mysqli_query($db,$student_details_sql);
  $student_details_result = mysqli_fetch_assoc($student_details_query);
}
?>
<div class="container">
    <div class="row">
      <div class="col-md-12"><h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Edit Student Details</h5>
      </div>
        <div class="col-md-3">
        </div>
        <div class="col-md-6">
          <form class="" action="student_profile_edit_p.php" method="post">
              <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Password</label>
                  <div class="col-sm-6">
                      <input type="password" name="password" class="form-control" value="<?php echo $user_pass_result['password'];?>"  required >
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-4 col-form-label">E:mail</label>
                  <div class="col-sm-6">
                      <input type="email" name="email" class="form-control" value="<?php echo $student_details_result['email'];?>"  required >
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Phone</label>
                  <div class="col-sm-6">
                      <input type="text" name="phone" class="form-control" value="<?php echo $student_details_result['phone'];?>"  required >
                  </div>
              </div>
              <div class="form-group row">
                  <label class="col-sm-4 col-form-label">Current Address</label>
                  <div class="col-sm-6">
                      <input type="text" name="current_address" class="form-control" value="<?php echo $student_details_result['current_address'];?>"  required >
                  </div>
              </div>
              <div class="col-md-3">
              </div>
              <div class="row">
                <div class="col-md-8 offset-md-4">
                    <div class="form-group">
                        <input type="hidden" name="student_id" value="<?php echo $student_id;?>">
                        <input type="submit" name="submit" class="btn btn-outline-primary btn-md" value="Submit">
                    </div>
                </div>
              </div>
          </form>
        </div>
    </div>
  </div>
</div>
<?php include_once 'footer.php';?>
