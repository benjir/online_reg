<?php
//session_start();
include_once 'session.php';
include_once 'database.php';
include_once 's_header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();
$student_id = $_SESSION['u_id'];
if ($db) {
  $student_dept_sql = "SELECT dept_id from student where u_id = '$student_id' ";
  $student_dept_query = mysqli_query($db,$student_dept_sql);
  if (mysqli_num_rows($student_dept_query) > 0 ) {
     $student_dept_result = mysqli_fetch_assoc($student_dept_query);
     $student_dept_id = $student_dept_result['dept_id'];
   }
 }

// $sn = 1;
$student_details_sql = "SELECT * FROM student WHERE u_id = '$student_id' ";
$student_details_query = mysqli_query($db,$student_details_sql);
$student_details_result = mysqli_fetch_assoc($student_details_query);
$student_details_semester = $student_details_result['semester'];
$student_dept_id = $student_details_result['dept_id'];
$student_dept_sql = "SELECT * FROM dept WHERE id = '$student_dept_id' ";
$student_dept_query = mysqli_query($db,$student_dept_sql);
$student_dept_result = mysqli_fetch_assoc($student_dept_query);
$student_prog_id = $student_details_result['prog_id'];
$student_prog_sql = "SELECT * FROM program WHERE id = '$student_prog_id' ";
$student_prog_query = mysqli_query($db,$student_prog_sql);
$student_prog_result = mysqli_fetch_assoc($student_prog_query);
$student_session_id = $student_details_result['session_id'];
$student_session_sql = "SELECT * FROM session WHERE id = '$student_session_id' ";
$student_session_query = mysqli_query($db,$student_session_sql);
$student_session_result = mysqli_fetch_assoc($student_session_query);
?>
<div class="container">
    <div class="row">
      <div class="col-md-12"><h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Student Details</h5></div>
        <div class="col-md-12 mt-3">
          <?php
          if (isset($_GET['sm']) && $_GET['sm'] == "success") {
            echo "<div class='alert alert-success text-center'><strong>Profile Edited Successfully !!</strong></div>";
          }
          ?>
        </div>
          <div class="col-md-4 offset-2">
              <p>ID Number: <?php echo $student_details_result['u_id'];?></p>
              <p>Name of Student: <?php echo $student_details_result['name'];?></p>
              <p>Name of Guardian: <?php echo $student_details_result['guardian_name'];?></p>
              <p>Mailing Address : <?php echo $student_details_result['current_address'];?></p>
              <p>Permanent Address : <?php echo $student_details_result['permanent_address'];?></p>
              <p>Contact No : <?php echo $student_details_result['phone'];?></p>
          </div>
          <div class="col-md-6">
              <p>Email : <?php echo $student_details_result['email'];?></p>
              <p>Relationship With Guardian : <?php echo $student_details_result['guardian_rel'];?></p>
              <p>Name of Dept : <?php echo $student_dept_result['name'];?></p>
              <p>Program : <?php echo $student_prog_result['name'];?></p>
              <p>Semester : <?php echo $student_details_semester;?></p>
              <p>Year : <?php echo $student_session_result['year'];?></p>
          </div>
          <div class="col-md-7 offset-5 mt-5">
            <a href="student_profile_edit.php?id=<?php echo $student_id;?>" class="btn btn-outline-primary btn-lg">Edit Profile</a>
          </div>
    </div>
</div>

<?php include_once 'footer.php';?>