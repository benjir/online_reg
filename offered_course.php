<?php
//session_start();
include_once 'session.php';
include_once 'database.php';
include_once 's_header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();
$student_id = $_SESSION['u_id'];
$student_syllabus_sql = "SELECT syllabus_id from student where u_id = '$student_id' ";
$student_syllabus_query = mysqli_query($db,$student_syllabus_sql);
$student_syllabus_result = mysqli_fetch_assoc($student_syllabus_query);
$student_syllabus_id = $student_syllabus_result['syllabus_id'];
// var_dump($student_syllabus_id);
// exit();
  $student_dept_sql = "SELECT dept_id from student where u_id = '$student_id' ";
  $student_dept_query = mysqli_query($db,$student_dept_sql);
  if(mysqli_num_rows($student_dept_query) > 0 ) {
   $student_dept_result = mysqli_fetch_assoc($student_dept_query);
   $student_dept_id = $student_dept_result['dept_id'];
  }
$sn = 1;
$ns = 1;

$student_pre_reg_sql = "SELECT * FROM student WHERE u_id = '$student_id' ";
$student_pre_reg_query = mysqli_query($db,$student_pre_reg_sql);
$student_pre_reg_result = mysqli_fetch_assoc($student_pre_reg_query);

//check if any result row exist
$student_pending_course_sql = "SELECT course_id,type_id from course_registration where u_id = '$student_id' AND status = 'PENDING' ";
$student_pending_course_query = mysqli_query($db,$student_pending_course_sql);
?>
<!--  start view added course list-->
<?php if (mysqli_num_rows($student_pending_course_query) > 0 ) : ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
              <!--  Current Semester Course Table Ends -->
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Preregistered Courses</h5>
            <?php
            if (isset($_GET['dm']) && $_GET['dm'] == "delete") {
              echo "<div class='alert alert-danger text-center'><strong>Course Deleted Successfully!!</strong></div>";
            }
            if (isset($_GET['ct']) && $_GET['ct'] == "course_taken") {
              echo "<div class='alert alert-danger text-center'><strong>This Course Already Taken!!</strong></div>";
            }
            if (isset($_GET['pre_reg']) && $_GET['pre_reg'] == "success") {
              echo "<div class='alert alert-success text-center'><strong>Pre-Registration Completed Successfully!!</strong></div>";
            }
            ?>
            <!--  Registered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Credit</th>
                          <th scope="col">Course Type</th>
                          <th scope="col">Course Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        $student_pending_course_sql = "SELECT course_id,type_id from course_registration where u_id = '$student_id' AND status = 'PENDING' ";
                        $student_pending_course_query = mysqli_query($db,$student_pending_course_sql);
                        if (mysqli_num_rows($student_pending_course_query) > 0 ) {
                           while($student_pending_course_result = mysqli_fetch_assoc($student_pending_course_query)){
                            $student_pending_course_id = $student_pending_course_result['course_id'];
                            $student_pending_course_type_id = $student_pending_course_result['type_id'];
                            $course_details_sql = "SELECT * from course where id = '$student_pending_course_id' AND dept_id = '$student_dept_id' ";
                            $course_details_query = mysqli_query($db,$course_details_sql);
                            if (mysqli_num_rows($course_details_query) > 0 ) {
                                while($course_details_query_result = mysqli_fetch_assoc($course_details_query)){
                                  ?>
                                <tr>
                                  <th scope="row"><?php echo $sn++;?></th>
                                  <td><?php echo $course_details_query_result['name'];?></td>
                                  <td><?php echo $course_details_query_result['code'];?></td>
                                  <td><?php echo $course_details_query_result['credit'];?></td>
                                  <td>
                                    <?php
                                    $course_type_name_sql = "SELECT type from type where id = '$student_pending_course_type_id' ";
                                    $course_type_name_query = mysqli_query($db,$course_type_name_sql);
                                    if (mysqli_num_rows($course_type_name_query) > 0 ) {
                                       $course_type_name_result = mysqli_fetch_assoc($course_type_name_query);
                                       echo $course_type_name_result['type'];
                                     }
                                    ?>
                                  </td>
                                  <td>
                                    <?php
                                    $course_status_sql = "SELECT status from course_registration where course_id = '$student_pending_course_id' AND u_id='$student_id'";
                                    $course_status_query = mysqli_query($db,$course_status_sql);
                                    if (mysqli_num_rows($course_status_query) > 0 ) {
                                       $course_status_result = mysqli_fetch_assoc($course_status_query);
                                       echo $course_status_result['status'];
                                     }
                                    ?>
                                  </td>
                                  <td>
                                    <a class="btn btn-outline-primary <?php
                                      if($student_pre_reg_result['pre_reg_complete']=='YES'){
                                        echo "disabled";
                                      }?>" href="delete_offered_added_course.php?id=<?php echo $student_pending_course_id;?>" onclick="return confirm('Are u sure want to delete this?')">Delete</a>
                                  </td>
                                </tr>
                                <?php }
                              }
                            }
                          } else {
                            echo '<p class="text-center"> No Course Added Yet!</p>';
                          }
                          ?>
                      </tbody>
                  </table>
                  <div class="col-md-5 offset-5 mb-2">
                    <a class="btn btn-outline-primary <?php if($student_pre_reg_result['pre_reg_complete']=='YES'){ echo "disabled";}?>" href="student_pre_reg_status.php?sid=<?php echo $student_id;?>">Submit</a>
                  </div>
              </div>
              <!--  Registered Course Table Ends -->
        </div>
    </div>
</div>
<?php endif ?>
<!--  end view added course list-->
<!--  Start offered course list-->

<div class="container">
  <div class="row">
      <div class="col-md-12">
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Offered Courses</h5>
            <?php
            if (isset($_GET['sm']) && $_GET['sm'] == "success") {
              echo "<div class='alert alert-success text-center'><strong>Course Added Successfully !!</strong></div>";
            }
            if (isset($_GET['fm']) && $_GET['fm'] == "failed") {
              echo "<div class='alert alert-danger text-center'><strong>Pre-requisite Not Completed!!</strong></div>";
            }
            if (isset($_GET['reg_done']) && $_GET['reg_done'] == "complete") {
              echo "<div class='alert alert-danger text-center'><strong>Can Not Add Course !! Preregistration Already Completed!!</strong></div>";
            }
            ?>
            <!--  Offered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Course Credit</th>
                          <th scope="col">Course Type</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($db) {
                          $course_sql = "SELECT * from course where dept_id = '$student_dept_id' AND syllabus_id='$student_syllabus_id' AND is_offered='YES' ";
                          $course_query = mysqli_query($db,$course_sql);
                          if (mysqli_num_rows($course_query) > 0 ) {
                              while ($course_result = mysqli_fetch_assoc($course_query)){
                                $course_id = $course_result['id'];
                        ?>
                        <tr>
                          <th scope="row"><?php echo $ns++;?></th>
                          <td><?php echo $course_result['name'];?></td>
                          <td><?php echo $course_result['code'];?></td>
                          <td><?php echo $course_result['credit'];?></td>
                          <td>
                            <div class="col-sm-8">
                            <form class="" action="offered_course_p.php" method="post">
                                <div class="form-group">
                                    <select name="type" class="form-control" id="exampleFormControlSelect1" required >
                                        <!-- <option>Select Type</option> -->
                                        <?php
                                        $type_sql = "SELECT * FROM type ";
                                        $type_query = mysqli_query($db,$type_sql);
                                        if (mysqli_num_rows($type_query) > 0) {
                                            while ( $type_result = mysqli_fetch_assoc($type_query)){
                                            ?>
                                            <option value="<?php echo $type_result['id']; ?>"> <?php echo $type_result['type'];?></option>
                                        <?php
                                          }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                          </td>
                        <td>
                            <div class="form-group">
                                <input type="hidden" name="course_id" value="<?php echo $course_id; ?>">
                                <input type="hidden" name="dept_id" value="<?php echo $student_dept_id; ?>">
                                <input type="submit" name="register" class="btn btn-outline-primary" value="Add">
                            </div>
                          </form>
                        </td>
                      </tr>
                    <?php } } ?>
                    </tbody>
                </table>
            </div>
            <!--  Offered Course Table Ends -->
                            <!-- </form>
                          </td>
                        </tr>
                      <?php   ?>
                      </tbody>
                  </table>
              </div> -->
              <!--  Offered Course Table Ends -->
            <?php } else { ?>
              <p class="text-center text-danger">Pre Registration Ended</p>
            <?php } ?>
      </div>
  </div>
</div>
<?php include_once 'footer.php';?>
