<?php
//session_start();
include_once 'session.php';
include_once 'database.php';
include_once 's_header.php';
if (!isLoggedIn()) {
    header("Location: index.php");
}
$db = db_connect();
$student_id = $_SESSION['u_id'];
//var_dump($user_id);
if ($db) {
  $student_dept_sql = "SELECT * from student where u_id = '$student_id' ";
  $student_dept_query = mysqli_query($db,$student_dept_sql);
  $student_dept_result = mysqli_fetch_assoc($student_dept_query);
  $student_dept_id = $student_dept_result['dept_id'];
  $student_syllabus_id = $student_dept_result['syllabus_id'];
 }
$sn = 1;
?>
<div class="container">
  <div class="row">
    <div class="col-md-12">

    </div>

  </div>

</div>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Course List</h5>
            <?php
            if (isset($_GET['sm']) && $_GET['sm'] == "success") {
              echo "<div class='alert alert-success'><strong>Course Offered For This Semester!!</strong></div>";
            }
            ?>
            <!--  Offered Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Course Name</th>
                          <th scope="col">Course Code</th>
                          <th scope="col">Course Credit</th>
                          <th scope="col">Prerequisite Courses</th>
                          <!-- <th scope="col">Course Offered</th> -->
                          <!-- <th scope="col">Action</th> -->
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($db) {
                          $course_sql = "SELECT * from course where dept_id = '$student_dept_id' AND status = 'ACTIVE' AND syllabus_id=$student_syllabus_id";
                          $course_query = mysqli_query($db,$course_sql);
                        }
                           if (mysqli_num_rows($course_query) > 0 ) {
                              while ($course_result = mysqli_fetch_assoc($course_query)){
                            ?>
                        <tr>
                          <th scope="row"><?php echo $sn ++;?></th>
                          <td><?php echo $course_result['name'];?></td>
                          <td><?php echo $course_result['code'];?></td>
                          <td><?php echo $course_result['credit'];?></td>
                          <td><?php echo $course_result['pre_course'];?></td>
                        </tr>
                      <?php } } ?>
                      </tbody>
                  </table>
              </div>
              <!--  Offered Course Table Ends -->
        </div>
    </div>
</div>
<?php include_once 'footer.php';?>
