<?php
   session_start();
   session_unset($_SESSION['u_id']);
   if(session_destroy()) {
      header("Location: index.php");
   }
?>
