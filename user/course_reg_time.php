<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 'header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
?>
<div class="container">
    <div class="row">
      <div class="col-md-3">
      </div>
      <div class="col-md-9">
        <?php
        if (isset($_GET['sm']) && $_GET['sm'] == "success") {
          echo "<div class='alert alert-success mt-2 text-center'><strong>Notice with date created successful! </strong></div>";
        }
        ?>
        <form class="" action="course_reg_time_p.php" method="post">
        <div class="form-group mt-5 row">
          <label class="col-sm-4" for="exampleFormControlTextarea1">Pre-registration Notice </label>
          <textarea class="form-control col-sm-4" name="notice_msg" id="exampleFormControlTextarea1"></textarea>
        </div>
        <div class="form-group row">
          <label class="col-sm-4" for="exampleFormControlInput1">Starting Date</label>
          <input type="date" name="start_date" class="form-control col-sm-4" id="exampleFormControlInput1" placeholder="name@example.com">
        </div>
        <div class="form-group row">
          <label class="col-sm-4" for="exampleFormControlInput1">Ending Date</label>
          <input type="date" name="end_date" class="form-control col-sm-4" id="exampleFormControlInput1" placeholder="name@example.com">
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Department</label>
                <div class="form-group col-sm-4">
                    <select name="dept_id" class="form-control" id="exampleFormControlSelect1" required >
                        <option value="">Select Department</option>
                        <?php
                        if ($db) {
                            $dept_sql = "SELECT * FROM dept ";
                            $dept_query = mysqli_query($db,$dept_sql);
                          }
                        if (mysqli_num_rows($dept_query) > 0){
                            while ( $dept_result = mysqli_fetch_assoc($dept_query)){
                        ?>
                            <option value="<?php echo $dept_result['id']; ?>"> <?php echo $dept_result['name'];?> </option>
                        <?php
                            } }
                        ?>
                    </select>
                </div>
          </div>
          <div class="form-group row">
              <label class="col-sm-4 col-form-label">Semester</label>
                  <div class="form-group col-sm-4">
                      <select name="semester_id" class="form-control" id="exampleFormControlSelect1" required >
                          <option value="">Select Semester</option>
                          <?php
                          $semester_sql = "SELECT * FROM semester ";
                          $semester_query = mysqli_query($db,$semester_sql);
                          if (mysqli_num_rows($semester_query) > 0){
                              while ( $semester_result = mysqli_fetch_assoc($semester_query)){
                          ?>
                              <option value="<?php echo $semester_result['id']; ?>"> <?php echo $semester_result['name'];?> </option>
                          <?php
                              } }
                          ?>
                      </select>
                  </div>
            </div>
            <div class="container">
                <div class="row">
                  <div class="col-md-3">
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                         <input type="submit" name="submit" class="btn btn-outline-primary btn-lg btn-block" value="Submit">
                    </div>
                  </div>
                  <div class="col-md-6">
                  </div>
                </div>
              </form>
            </div>
      </div>
    </div>
</div>
<?php include_once 'footer.php';?>
