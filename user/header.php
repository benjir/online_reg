
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Pre Registration</title>
    <!-- Bootstrap -->
    <link rel="shortcut icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <link rel="icon" href="../assets/img/favicon.ico" type="image/x-icon">
    <link href="../assets/css/bootstrap/bootstrap.min.css" rel="stylesheet">
    <link href="../assets/css/style.css" rel="stylesheet">
    <link href="../assets/css/font-awesome/font-awesome.min.css" rel="stylesheet" integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg==" crossorigin="anonymous">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
      <!---Header Start---------->
      <header class="header">
          <div class="container">
              <div class="row">
                  <div class="col-md-8">
                      <div class="logo"><a href="../index.php"><img src="../assets/img/logo.png"></a></div>
                      <div class="headline"><h3>STATE UNIVERSITY OF BANGLADESH</h3></div>
                  </div>
                  <div class="col-md-4">
                      <div class="phone">
                          <i class="fa fa-phone"></i> +900 6584 4858
                      </div>
                  </div>
              </div>
          </div>
      </header>
                                   <!--Header End------------>
                             <!---Navigation Menu Start------------>
      <div class="container">
          <div class="row">
              <div class="col-md-12">
                  <nav class="navbar navbar-expand-lg navbar-light" style="background-color: #E8E8F1;">
                      <ul class="navbar-nav">
                        <li class="nav-item">
                          <a class="nav-link" href="user_dashboard.php"><strong>Profile</strong></a>
                        </li>
                        <li class="nav-item">
                          <a class="nav-link" href="teacher_list.php"><strong>Teachers List</strong></a>
                        </li>
                          <li class="nav-item">
                            <a class="nav-link" href="student_list.php"><strong>Students List</strong></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="course_reg_time.php"><strong>Preregistration Notice</strong></a>
                          </li>
                          <!-- <li class="nav-item">
                            <a class="nav-link" href="notice.php"><strong>Notice</strong></a>
                          </li> -->
                          <li class="nav-item">
                            <a class="nav-link" href="assign_advisor.php"><strong>Assign Advisor</strong></a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" href="logout.php"><strong>Logout</strong></a>
                          </li>
                      </ul>
                  </nav>
              </div>
          </div>
      </div>
      <!--Navigation Menu Ends------------>
