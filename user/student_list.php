<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 'header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();
$sn = 1;
?>
<div class="container">
    <div class="row">
       <div class="col-md-12 mt-5">
          <a href="student_reg.php" class="btn btn-outline-primary"><strong>REGISTER NEW STUDENT</strong></a>
       </div>
    </div>
    <div class="row">
        <div class="col-md-12">
          <?php
          if (isset($_GET['sa']) && $_GET['sa'] == "active") {
            echo "<div class='mt-2 mb-2 alert alert-success text-center'><strong>STUDENT STATUS ACTIVATED!</strong></div>";
          }
          if (isset($_GET['sd']) && $_GET['sd'] == "deactive") {
            echo "<div class='mt-2 mb-2 alert alert-danger text-center'><strong>STUDENT STATUS DEACTIVATED!</strong></div>";
          }
          ?>
            <h5 class="text-center p-3 mb-2 mt-2 bg-secondary text-white">Students List</h5>
            <!--  All Course Table Start -->
              <div class="table-responsive-md">
                  <table class="table table-bordered">
                    <thead class="thead-light">
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">ID</th>
                          <th scope="col">Status</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if ($db) {
                          $user_sql = "SELECT * from user WHERE user_role='STUDENT'";
                          $user_query = mysqli_query($db,$user_sql);
                        }
                           if (mysqli_num_rows($user_query) > 0 ) {
                              while ($user_result = mysqli_fetch_assoc($user_query)){
                              // echo '<pre>';
                              // echo '</pre>';
                              // print_r($course_result);
                        ?>
                        <tr>
                          <th scope="row"><?php echo $sn ++;?></th>
                          <td><?php echo $user_result['u_id'];?></td>
                          <td><?php echo $user_result['status'];?></td>
                          <td>
                              <div class="form-group">
                                  <?php if ($user_result['status'] == 'DEACTIVE') { ?>
                                    <a class="btn btn-outline-primary btn-sm" href="student_activation.php?status=<?php echo $user_result['status'];?>&u_id=<?php echo $user_result['u_id'];?>">ACTIVE</a>
                                  <?php } else { ?>
                                    <a class="btn btn-outline-primary btn-sm" href="student_activation.php?status=<?php echo $user_result['status'];?>&u_id=<?php echo $user_result['u_id'];?>">DEACTIVE</a>
                                  <?php } ?>
                              </div>
                          </td>
                        </tr>
                      <?php } }?>
                      </tbody>
                  </table>
              </div>
              <!--  All Course Table Ends -->

        </div>
    </div>
</div>
<?php include_once 'footer.php' ;?>
