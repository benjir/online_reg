<?php
//session_start();
include_once '../session.php';
include_once '../database.php';
include_once 'header.php';
if (!isLoggedIn()) {
    header("Location: ../index.php");
}
$db = db_connect();

?>
<div class="container">
    <div class="row">
      <div class="col-md-12">
        <h5 class="text-center p-3 mb-5 mt-2 bg-secondary text-white">Assign Teacher</h5>
      </div>
      <div class="col-md-3">
      </div>
      <div class="col-md-6 mt-5">
        <?php
        if (isset($_GET['sm']) && $_GET['sm'] == "success") {
          echo "<div class='alert alert-success mt-2 text-center'><strong>Teacher assigned successful! </strong></div>";
        }
        ?>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Select Teacher</label>
            <div class="col-sm-8">
              <form class="" action="assign_advisor_p.php" method="post">
                <div class="form-group">
                    <select name="teacher_id" class="form-control" id="exampleFormControlSelect1" required >
                        <option>Select Teacher</option>
                        <?php
                        if ($db) {
                            $teacher_sql = "SELECT * FROM teacher ";
                            $teacher_query = mysqli_query($db,$teacher_sql);
                        }
                        if (mysqli_num_rows($teacher_query) > 0) {
                            while ( $teacher_result = mysqli_fetch_assoc($teacher_query)){
                            ?>
                            <option value="<?php echo $teacher_result['id']; ?>"> <?php echo $teacher_result['name'];?></option>
                        <?php
                            } }
                        ?>
                    </select>
                </div>
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Department</label>
            <div class="col-sm-8">
                <div class="form-group">
                    <select name="dept_id" class="form-control" id="exampleFormControlSelect1" required >
                        <option value="">Select Department</option>
                        <?php
                        if ($db) {
                            $dept_sql = "SELECT * FROM dept ";
                            $dept_query = mysqli_query($db,$dept_sql);
                          }
                        if (mysqli_num_rows($dept_query) > 0){
                            while ( $dept_result = mysqli_fetch_assoc($dept_query)){
                        ?>
                            <option value="<?php echo $dept_result['id']; ?>"> <?php echo $dept_result['name'];?> </option>
                        <?php
                            } }
                        ?>
                    </select>
                </div>
            </div>
          </div>
            <div class="form-group row">
                <label class="col-sm-4 col-form-label">Batch No</label>
                <div class="col-sm-8">
                    <div class="form-group">
                        <select name="batch_id" class="form-control" id="exampleFormControlSelect1" required >
                            <option>Select Batch</option>
                            <?php
                            if ($db) {
                                $batch_sql = "SELECT * FROM batch ";
                                $batch_query = mysqli_query($db,$batch_sql);
                            }
                            if (mysqli_num_rows($batch_query) > 0) {
                                while ( $batch_result = mysqli_fetch_assoc($batch_query)){
                                ?>
                                <option value="<?php echo $batch_result['id']; ?>"> <?php echo $batch_result['batch_no'];?></option>
                            <?php
                                } }
                            ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
        </div>
      </div>
    </div>
    <div class="container">
        <div class="row">
          <div class="col-md-5">

          </div>
          <div class="col-md-3">
            <div class="form-group">
                 <input type="submit" name="submit" class="btn btn-outline-primary btn-lg btn-block" value="Assign Teacher">
            </div>
          </div>
          <div class="col-md-4">
          </div>
        </div>
      </form>
    </div>
</div>
<?php include_once 'footer.php';?>
