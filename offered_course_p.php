<?php
include_once 'database.php';
include_once 'session.php';
$db = db_connect();
// print_r($_POST);
// exit();
if (isset($_POST['register'])){
  //$student_id = $_POST['u_id'];
  $student_id = $_SESSION['u_id'];
  $student_pre_reg_sql = "SELECT * FROM student WHERE u_id = '$student_id' ";
  $student_pre_reg_query = mysqli_query($db,$student_pre_reg_sql);
  $student_pre_reg_result = mysqli_fetch_assoc($student_pre_reg_query);
  if ($student_pre_reg_result['pre_reg_complete']=='YES') {
    // echo "in the if";
    header("Location: offered_course.php?reg_done=complete");
    // exit();

  } else{
      $student_batch_sql = "SELECT batch_id from student where u_id = '$student_id' ";
      $student_batch_query = mysqli_query($db,$student_batch_sql);
      if (mysqli_num_rows($student_batch_query) > 0 ) {
         $student_batch_result = mysqli_fetch_assoc($student_batch_query);
         $student_batch_id = $student_batch_result['batch_id'];
         $teacher_id_sql = "SELECT teacher_id from teacher_batch where batch_id = '$student_batch_id' ";
         $teacher_id_query = mysqli_query($db,$teacher_id_sql);
         if (mysqli_num_rows($teacher_id_query) > 0 ) {
            $teacher_id_result = mysqli_fetch_assoc($teacher_id_query);
            $teacher_id = $teacher_id_result['teacher_id'];
       }
      $course_id = $_POST['course_id'];
      $student_dept_id = $_POST['dept_id'];
      $type_id = $_POST['type'];

      $check_pre_requisite_sql = "SELECT * FROM pre_course WHERE course_id = $course_id";
      $check_pre_requisite_query = mysqli_query($db,$check_pre_requisite_sql);
      $total_pre_requisite_courses = mysqli_num_rows($check_pre_requisite_query);

      $check_pre_requisite_completed_sql = "SELECT * FROM course_registration cr LEFT OUTER JOIN pre_course pc ON cr.course_id = pc.pre_course_id WHERE pc.course_id = $course_id";
      // echo $check_pre_requisite_completed_sql."<br>";
      $check_pre_requisite_completed_query = mysqli_query($db,$check_pre_requisite_completed_sql);
      $total_pre_requisite_courses_completed = mysqli_num_rows($check_pre_requisite_completed_query);

      if ($total_pre_requisite_courses == $total_pre_requisite_courses_completed) {
        $check_course_if_taken_sql = "SELECT course_id,u_id FROM course_registration WHERE course_id='$course_id' AND u_id='$student_id'";
        $check_course_if_taken_query = mysqli_query($db,$check_course_if_taken_sql);
        $check_course_row = mysqli_num_rows($check_course_if_taken_query);
        // print_r($check_course_row);
        // exit();
         if ($check_course_row > 0) {
          header("Location: offered_course.php?ct=course_taken");
        } else{
          $course_reg_sql = "INSERT INTO course_registration (u_id,course_id,dept_id,type_id,teacher_id) VALUES ('$student_id','$course_id','$student_dept_id','$type_id','$teacher_id') ";
          //echo $course_reg_sql;
              if (mysqli_query($db,$course_reg_sql)) {
                header("Location: offered_course.php?sm=success");
              }
            }
          } else {
              header("Location: offered_course.php?fm=failed");
          }
      } else {
      echo "Can not Register Subject".mysqli_connect_error();
      }
    }
    }