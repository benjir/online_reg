-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 07, 2019 at 09:16 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `student_online_reg`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE `batch` (
  `id` int(11) NOT NULL,
  `batch_no` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`id`, `batch_no`) VALUES
(1, 30),
(2, 31),
(3, 32),
(4, 33),
(5, 34),
(6, 35),
(7, 36),
(8, 37),
(9, 38),
(10, 39),
(11, 40),
(12, 41),
(13, 42),
(14, 43),
(15, 44),
(16, 45);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE `course` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `code` varchar(50) NOT NULL,
  `credit` decimal(13,2) NOT NULL,
  `pre_course` varchar(100) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'ACTIVE',
  `is_offered` varchar(50) NOT NULL DEFAULT 'NO',
  `dept_id` int(11) NOT NULL,
  `syllabus_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `name`, `code`, `credit`, `pre_course`, `status`, `is_offered`, `dept_id`, `syllabus_id`) VALUES
(1, 'Computer Fundamental', 'CSE-1101', '3.00', 'cse-101', 'ACTIVE', 'YES', 1, 1),
(2, 'Data Structure', 'CSE-2101', '3.00', '', 'ACTIVE', 'YES', 1, 1),
(3, 'Algorithm', 'CSE-3101', '3.00', '', 'ACTIVE', 'YES', 1, 1),
(4, 'Computer Network', 'CSE-4101', '3.00', 'cse-103', 'ACTIVE', 'NO', 1, 1),
(5, 'VLSI', 'CSE-3201', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(6, 'DLD', 'CSE-2201', '2.00', '', 'ACTIVE', 'NO', 1, 1),
(7, 'Mathemathics', 'MATH-1201', '2.00', 'cse-205', 'ACTIVE', 'NO', 1, 1),
(8, 'English (1)', 'ENG-1301', '2.00', '', 'ACTIVE', 'NO', 1, 1),
(9, 'Parmacy 1', 'PHA-2120', '4.00', '', 'ACTIVE', 'YES', 5, 1),
(10, 'LAW 1', 'LAW-1301', '2.00', '', 'ACTIVE', 'YES', 2, 1),
(11, 'Mathematics-I ', 'MATH 191', '3.00', 'Remedial Math', 'ACTIVE', 'NO', 1, 1),
(12, 'English-I ', 'ENG 171', '3.00', 'Remedial English', 'ACTIVE', 'NO', 1, 1),
(13, 'Mathematics-II ', 'MATH 193 ', '3.00', 'Math 191', 'ACTIVE', 'NO', 1, 1),
(14, 'English-II ', 'ENG 173', '3.00', 'ENG 171', 'ACTIVE', 'NO', 1, 1),
(15, 'Introduction to Electrical Engineering', 'EEE 105', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(16, 'Introduction to Electrical Engineering Sessional', 'EEE 106', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(17, 'Mathematics-III ', 'MATH 195', '3.00', 'MATH 193', 'ACTIVE', 'NO', 1, 1),
(18, 'Physics ', 'PHY 109', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(19, 'Basic Electronics', 'EEE 205', '3.00', 'EEE 105, EEE 106', 'ACTIVE', 'NO', 1, 1),
(20, 'Basic Electronics Sessional', 'EEE 206', '1.50', 'EEE 105, EEE 106', 'ACTIVE', 'NO', 1, 1),
(21, 'Basic Mechanical Engineering', 'ME 107', '3.00', 'EEE 105, EEE 106', 'ACTIVE', 'NO', 1, 1),
(22, 'Mathematics-IV ', 'MATH 197', '3.00', 'MATH 195', 'ACTIVE', 'NO', 1, 1),
(23, 'Electrical Drives and Instrumentation', 'EEE 213', '3.00', 'EEE 105', 'ACTIVE', 'NO', 1, 1),
(24, 'Mathematics-V ', 'MATH 199', '3.00', 'MATH 195', 'ACTIVE', 'NO', 1, 1),
(25, 'Digital Electronics and Pulse Techniques', 'EEE301', '3.00', 'CSE 207', 'ACTIVE', 'NO', 1, 1),
(26, 'Financial and Managerial Accounting', 'ACT 311', '2.00', '', 'ACTIVE', 'NO', 1, 1),
(27, 'Sociology', 'SCO 412  ', '2.00', 'ENG 173', 'ACTIVE', 'NO', 1, 1),
(28, 'Business Communication', 'BUC 311', '2.00', '', 'ACTIVE', 'NO', 1, 1),
(29, 'Economics', 'ECO111', '2.00', '', 'ACTIVE', 'NO', 1, 1),
(30, 'Industrial Management', 'MGT 411', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(31, 'Computer Fundamentals', 'CSE 101', '2.00', '', 'ACTIVE', 'NO', 1, 1),
(32, 'Computer Fundamentals Sessional', 'CSE 102', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(33, 'Structured Programming Language', 'CSE 103', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(34, 'Structured Programming Language Sessional', 'CSE 104', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(35, 'Discrete Mathematics', 'CSE 201', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(36, 'Object Oriented Programming Language', 'CSE 203', '2.00', 'CSE 103', 'ACTIVE', 'NO', 1, 1),
(37, 'Object Oriented Programming Language Sessional', 'CSE 204 ', '1.50', 'CSE 103', 'ACTIVE', 'NO', 1, 1),
(38, 'Digital Logic Design', 'CSE 207', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(39, 'Digital Logic Design Sessional', 'CSE 208', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(40, 'Data Structures', 'CSE 209', '3.00', 'CSE 103', 'ACTIVE', 'NO', 1, 1),
(41, 'Numerical Methods', 'CSE 211', '2.00', '', 'ACTIVE', 'NO', 1, 1),
(42, 'Algorithms', 'CSE 215', '3.00', 'CSE201, CSE209', 'ACTIVE', 'NO', 1, 1),
(43, 'Algorithms Sessional', 'CSE 216', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(44, 'Theory of Computation', 'CSE 303', '2.00', 'CSE 201', 'ACTIVE', 'NO', 1, 1),
(45, 'Database Systems', 'CSE 305 ', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(46, 'Database Systems Sessional', 'CSE 306', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(47, 'Computer Architecture', 'CSE 307', '3.00', 'EEE 205', 'ACTIVE', 'NO', 1, 1),
(48, 'Software Engineering', 'CSE 309', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(49, 'Compiler Design', 'CSE 311', '3.00', 'CSE 303', 'ACTIVE', 'NO', 1, 1),
(50, 'Compiler Design Sessional', 'CSE 312 ', '0.75', '', 'ACTIVE', 'NO', 1, 1),
(51, 'Data Communication', 'CSE 313', '3.00', 'MATH 199', 'ACTIVE', 'NO', 1, 1),
(52, 'Operating System', 'CSE 315', '3.00', 'CSE 215', 'ACTIVE', 'NO', 1, 1),
(53, 'Operating System Sessional', 'CSE 316', '0.75', 'CSE 215', 'ACTIVE', 'NO', 1, 1),
(54, 'Microprocessors and Microcontrollers', 'CSE 317 ', '3.00', 'EEE 205', 'ACTIVE', 'NO', 1, 1),
(55, 'Microprocessor and Microcontrollers Sessional', 'CSE 318', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(56, 'Artificial Intelligence', 'CSE 319', '3.00', 'CSE 215', 'ACTIVE', 'NO', 1, 1),
(57, 'Artificial Intelligence Sessional', 'CSE 320', '0.75', '', 'ACTIVE', 'NO', 1, 1),
(58, 'Project and Thesis', 'CSE 400', '3.50', '', 'ACTIVE', 'NO', 1, 1),
(59, 'Computer Networks', 'CSE 401', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(60, 'Computer Networks Sessional', 'CSE 402', '0.75', '', 'ACTIVE', 'NO', 1, 1),
(61, 'Digital System Design', 'CSE 403', '3.00', 'CSE 317', 'ACTIVE', 'NO', 1, 1),
(62, 'Digital System Design Sessional', 'CSE 404 ', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(63, 'System Analysis and Design ', 'CSE 405', '3.00', 'CSE 305', 'ACTIVE', 'NO', 1, 1),
(64, 'System Analysis and Design Sessional', 'CSE 406', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(65, 'Computer Interfacing', 'CSE 407', '3.00', 'CSE 307', 'ACTIVE', 'NO', 1, 1),
(66, 'Computer Interfacing Sessional', 'CSE 408', '0.75', '', 'ACTIVE', 'NO', 1, 1),
(67, 'Computer Graphics', 'CSE 409', '3.00', 'MATH 197, CSE 215', 'ACTIVE', 'NO', 1, 1),
(68, 'Computer Graphics Sessional', 'CSE 410 ', '0.75', '', 'ACTIVE', 'NO', 1, 1),
(69, 'VLSI Design', 'CSE 411', '3.00', 'EEE 301', 'ACTIVE', 'NO', 1, 1),
(70, 'VLSI Design Sessional', 'CSE 412', '1.50', '', 'ACTIVE', 'NO', 1, 1),
(71, 'Celluler and Mobile Communication', 'CSE 429', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(72, 'Network and Distributed Processing', 'CSE 447', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(73, 'E-Commerce', 'CSE 449', '3.00', '', 'ACTIVE', 'NO', 1, 1),
(74, 'Bangla Language', 'BAN-1101', '2.00', '', 'ACTIVE', 'NO', 1, 2),
(75, 'English Language', 'ENG-1101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(76, 'Professional Communications Lab', 'ENG-2102', '1.00', 'ENG-1101', 'ACTIVE', 'NO', 1, 2),
(77, 'Economics of Information Technology', 'GED-2101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(78, 'Financial and Managerial Accounting', 'GED-4101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(79, 'Bangladesh Studies', 'GED-3101', '2.00', '', 'ACTIVE', 'NO', 1, 2),
(80, 'Engineering Ethics', 'GED-4103', '2.00', '', 'ACTIVE', 'NO', 1, 2),
(81, 'Industrial & Operations Management', 'GED-3103', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(82, 'Chemistry', 'CHE-1101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(83, 'Physics', 'PHY-1101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(84, 'Physics Lab', 'PHY-1102', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(85, 'Differential Calculus & Coordinate Geometry', 'MAT-1101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(86, 'Integral Calculus & Differential Equations', 'MAT-2101', '3.00', 'MAT-1101', 'ACTIVE', 'NO', 1, 2),
(87, 'Linear Algebra, Vector Functions & Series Solution', 'MAT-2103', '3.00', 'MAT-2101', 'ACTIVE', 'NO', 1, 2),
(88, 'Method of Applied Mathematics', 'MAT-3101', '3.00', 'MAT-2103', 'ACTIVE', 'NO', 1, 2),
(89, 'Introduction to Probability and Statistics', 'STA-2101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(90, 'Electrical Circuit Analysis', 'EEE-1101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(91, 'Electrical Circuit Analysis Lab', 'EEE-1102', '1.50', '', 'ACTIVE', 'NO', 1, 2),
(92, 'Electronic Devices and Circuits', 'EEE-1103', '3.00', 'EEE-1101', 'ACTIVE', 'NO', 1, 2),
(93, 'Electronic Devices and Circuits Lab', 'EEE-1104', '1.50', 'EEE-1102', 'ACTIVE', 'NO', 1, 2),
(94, 'Engineering Drawing', 'APE-2101', '2.00', '', 'ACTIVE', 'NO', 1, 2),
(95, 'Computer Fundamentals', 'CSE-1101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(96, 'Computer Fundamentals Lab', 'CSE-1102', '1.50', '', 'ACTIVE', 'NO', 1, 2),
(97, 'Structured Programming Language', 'CSE-1103', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(98, 'Structured Programming Language Lab', 'CSE-1104', '1.50', '', 'ACTIVE', 'NO', 1, 2),
(99, 'Discrete Mathematics', 'CSE-1105', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(100, 'Object Oriented Programming Language', 'CSE-2101', '3.00', 'CSE-1103', 'ACTIVE', 'NO', 1, 2),
(101, 'Object Oriented Programming Language Lab', 'CSE-2102', '1.50', 'CSE-1104', 'ACTIVE', 'NO', 1, 2),
(102, 'Data Structure', 'CSE-2103', '3.00', 'CSE-2101', 'ACTIVE', 'NO', 1, 2),
(103, 'Data Structure Lab', 'CSE-2104', '1.00', 'CSE-2102', 'ACTIVE', 'NO', 1, 2),
(104, 'Theory of Computation', 'CSE-2105', '3.00', 'CSE-1105', 'ACTIVE', 'NO', 1, 2),
(105, 'Numerical Methods', 'CSE-2107', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(106, 'Numerical Methods Lab', 'CSE-2108', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(107, 'Digital Logic Design', 'CSE-2109', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(108, 'Digital Logic Design Lab', 'CSE-2110', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(109, 'Mobile App and Web Development', 'CSE-2111', '3.00', 'CSE-2101', 'ACTIVE', 'NO', 1, 2),
(110, 'Mobile App and Web Development Lab', 'CSE-2112', '1.50', 'CSE-2102', 'ACTIVE', 'NO', 1, 2),
(111, 'Project Work I', 'CSE-3100', '2.00', 'CSE-2102, CSE-2112', 'ACTIVE', 'NO', 1, 2),
(112, 'Algorithms', 'CSE-3101', '3.00', 'CSE-2103', 'ACTIVE', 'NO', 1, 2),
(113, 'Algorithms Lab', 'CSE-3102', '1.00', 'CSE-2104', 'ACTIVE', 'NO', 1, 2),
(114, 'Database Systems', 'CSE-3103', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(115, 'Database Systems Lab', 'CSE-3104', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(116, 'System Analysis and Design ', 'CSE-3105', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(117, 'System Analysis and Design Lab', 'CSE-3106', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(118, 'Microprocessor and Microcontroller', 'CSE-3107', '3.00', 'CSE-2109', 'ACTIVE', 'NO', 1, 2),
(119, 'Microprocessor and Microcontroller Lab', 'CSE-3108', '1.00', 'CSE-2110', 'ACTIVE', 'NO', 1, 2),
(120, 'Computer Architecture', 'CSE-3109', '3.00', 'CSE-2109', 'ACTIVE', 'NO', 1, 2),
(121, 'Software Engineering', 'CSE-3111', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(122, 'Software Engineering Lab', 'CSE-3112', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(123, 'Project and Thesis', 'CSE-4100', '4.00', '', 'ACTIVE', 'NO', 1, 2),
(124, 'Operating System', 'CSE-4101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(125, 'Operating System Lab', 'CSE-4102', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(126, 'Computer Networking', 'CSE-4103', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(127, 'Computer Networking Lab', 'CSE-4104', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(128, 'Computer and Cyber Security', 'CSE-4105', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(129, 'Artificial Intelligence', 'CSE-5101', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(130, 'Artificial Intelligence Lab', 'CSE-5102', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(131, 'Compiler Design', 'CSE-5103', '3.00', 'CSE-2105', 'ACTIVE', 'NO', 1, 2),
(132, 'Compiler Design Lab', 'CSE-5104', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(133, 'Computer Graphics', 'CSE-5105', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(134, 'Computer Graphics Lab', 'CSE-5106', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(135, 'Computer Vision and Image Processing', 'CSE-5107', '3.00', 'CSE-5105', 'ACTIVE', 'NO', 1, 2),
(136, 'Computer Vision and Image Processing Lab', 'CSE-5108', '1.00', 'CSE-5106', 'ACTIVE', 'NO', 1, 2),
(137, 'Computer Peripherals and Interfacing', 'CSE-5109', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(138, 'Computer Peripherals and Interfacing Lab', 'CSE-5110', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(139, 'Contemporary Course on Computer Science', 'CSE-5111', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(140, 'Contemporary Course on Computer Science Lab', 'CSE-5112', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(141, 'E-Commerce and Web Engineering', 'CSE-5113', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(142, 'E-Commerce and Web Engineering Lab', 'CSE-5114', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(143, 'Parallel Processing and Distributed System', 'CSE-5115', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(144, 'Parallel Processing and Distributed System Lab', 'CSE-5116', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(145, 'VLSI Design', 'CSE-5117', '3.00', '', 'ACTIVE', 'NO', 1, 2),
(146, 'VLSI Design Lab', 'CSE-5118', '1.00', '', 'ACTIVE', 'NO', 1, 2),
(147, 'Basic Graph Theory', 'CSE-6101', '2.00', '', 'ACTIVE', 'NO', 1, 2),
(148, 'Bioinformatics', 'CSE-6103', '2.00', '', 'ACTIVE', 'NO', 1, 2),
(149, 'Computational Geometry', 'CSE-6105', '2.00', '', 'ACTIVE', 'NO', 1, 2),
(150, 'Cryptography and Cryptology        ', 'CSE-6107', '2.00', '', 'ACTIVE', 'NO', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `course_registration`
--

CREATE TABLE `course_registration` (
  `id` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `type_id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'PENDING',
  `reg_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `course_reg_time`
--

CREATE TABLE `course_reg_time` (
  `id` int(11) NOT NULL,
  `notice` text NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `semester_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `course_reg_time`
--

INSERT INTO `course_reg_time` (`id`, `notice`, `start_date`, `end_date`, `semester_id`, `dept_id`) VALUES
(3, 'Pre-registration of CSE Dept will be started from 1-1-19 and will be ended on 20-1-19               ', '2019-01-01', '2019-01-20', 1, 1),
(4, 'Pre-registration of LAW Dept will be start from 20-12-18 and will be end on 28-12-18', '2019-01-01', '2019-01-01', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `dept`
--

CREATE TABLE `dept` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `dept`
--

INSERT INTO `dept` (`id`, `name`) VALUES
(1, 'CSE'),
(2, 'LAW'),
(3, 'ENGLISH'),
(4, 'BBA'),
(5, 'PHARMACY'),
(6, 'ARCHITECTURE');

-- --------------------------------------------------------

--
-- Table structure for table `pre_course`
--

CREATE TABLE `pre_course` (
  `id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `pre_course_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `program`
--

CREATE TABLE `program` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `program`
--

INSERT INTO `program` (`id`, `name`) VALUES
(1, 'B.Sc'),
(2, 'M.Sc');

-- --------------------------------------------------------

--
-- Table structure for table `semester`
--

CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `semester`
--

INSERT INTO `semester` (`id`, `name`, `start_date`, `end_date`) VALUES
(1, 'FALL', '2018-09-01', '2018-12-31'),
(2, 'SPRING', '2018-01-01', '2018-04-30'),
(3, 'SUMMER', '2018-05-01', '2018-08-31');

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` int(11) NOT NULL,
  `year` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `session`
--

INSERT INTO `session` (`id`, `year`) VALUES
(1, '2016'),
(2, '2017'),
(3, '2018'),
(4, '2019');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `pre_reg_complete` varchar(50) NOT NULL DEFAULT 'NO',
  `current_address` varchar(200) NOT NULL,
  `permanent_address` varchar(200) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `prog_id` int(11) NOT NULL,
  `session_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `syllabus_id` int(11) NOT NULL,
  `semester` varchar(50) NOT NULL,
  `activity_log` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `guardian_name` varchar(50) NOT NULL,
  `guardian_rel` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `syllabus`
--

CREATE TABLE `syllabus` (
  `id` int(11) NOT NULL,
  `version` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `syllabus`
--

INSERT INTO `syllabus` (`id`, `version`) VALUES
(1, 'A-2016'),
(2, 'B-2018'),
(3, 'C-2020'),
(4, 'D-2022');

-- --------------------------------------------------------

--
-- Table structure for table `teacher`
--

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `teacher_batch`
--

CREATE TABLE `teacher_batch` (
  `id` int(11) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `batch_id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

-- --------------------------------------------------------

--
-- Table structure for table `type`
--

CREATE TABLE `type` (
  `id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `type`
--

INSERT INTO `type` (`id`, `type`) VALUES
(1, 'G'),
(2, 'I'),
(3, 'R');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `u_id` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `user_role` varchar(50) NOT NULL DEFAULT 'STUDENT',
  `status` varchar(50) NOT NULL DEFAULT 'ACTIVE'
) ENGINE=InnoDB DEFAULT CHARSET=utf16;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `u_id`, `password`, `user_role`, `status`) VALUES
(1, '1001', '123', 'ADMIN', 'ACTIVE');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `batch`
--
ALTER TABLE `batch`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dept3_id` (`dept_id`),
  ADD KEY `fk_syllabus2_id` (`syllabus_id`);

--
-- Indexes for table `course_registration`
--
ALTER TABLE `course_registration`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_course_id` (`course_id`),
  ADD KEY `fk_u_id` (`u_id`),
  ADD KEY `fk_dept5_id` (`dept_id`),
  ADD KEY `fk_type_id` (`type_id`),
  ADD KEY `fk_teacher3_id` (`teacher_id`);

--
-- Indexes for table `course_reg_time`
--
ALTER TABLE `course_reg_time`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_dept4_id` (`dept_id`),
  ADD KEY `fk_semester_id` (`semester_id`);

--
-- Indexes for table `dept`
--
ALTER TABLE `dept`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pre_course`
--
ALTER TABLE `pre_course`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_course6_id` (`course_id`),
  ADD KEY `fk_course7_id` (`pre_course_id`),
  ADD KEY `fk_dept6_id` (`dept_id`);

--
-- Indexes for table `program`
--
ALTER TABLE `program`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `semester`
--
ALTER TABLE `semester`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_id` (`u_id`),
  ADD KEY `fk_dept_id` (`dept_id`),
  ADD KEY `fk_prog_id` (`prog_id`),
  ADD KEY `fk_session_id` (`session_id`),
  ADD KEY `fk_semester_id` (`semester`),
  ADD KEY `fk_batch_id` (`batch_id`),
  ADD KEY `fk_syllabus_id` (`syllabus_id`);

--
-- Indexes for table `syllabus`
--
ALTER TABLE `syllabus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher`
--
ALTER TABLE `teacher`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_id` (`u_id`),
  ADD KEY `fk_dept2_id` (`dept_id`);

--
-- Indexes for table `teacher_batch`
--
ALTER TABLE `teacher_batch`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_batch2_id` (`batch_id`),
  ADD KEY `fk_dept8_id` (`dept_id`),
  ADD KEY `fk_teacher4_id` (`teacher_id`);

--
-- Indexes for table `type`
--
ALTER TABLE `type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `u_id` (`u_id`) USING BTREE;

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `batch`
--
ALTER TABLE `batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT for table `course_registration`
--
ALTER TABLE `course_registration`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=93;

--
-- AUTO_INCREMENT for table `course_reg_time`
--
ALTER TABLE `course_reg_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `dept`
--
ALTER TABLE `dept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `pre_course`
--
ALTER TABLE `pre_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `program`
--
ALTER TABLE `program`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `semester`
--
ALTER TABLE `semester`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `session`
--
ALTER TABLE `session`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `student`
--
ALTER TABLE `student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `syllabus`
--
ALTER TABLE `syllabus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `teacher`
--
ALTER TABLE `teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `teacher_batch`
--
ALTER TABLE `teacher_batch`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `type`
--
ALTER TABLE `type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `fk_dept3_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_syllabus2_id` FOREIGN KEY (`syllabus_id`) REFERENCES `syllabus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course_registration`
--
ALTER TABLE `course_registration`
  ADD CONSTRAINT `fk_course_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dept5_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_teacher3_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_type_id` FOREIGN KEY (`type_id`) REFERENCES `type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_u_id` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `course_reg_time`
--
ALTER TABLE `course_reg_time`
  ADD CONSTRAINT `fk_dept4_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_semester_id` FOREIGN KEY (`semester_id`) REFERENCES `semester` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pre_course`
--
ALTER TABLE `pre_course`
  ADD CONSTRAINT `fk_course6_id` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_course7_id` FOREIGN KEY (`pre_course_id`) REFERENCES `course` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dept6_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `fk_batch_id` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_prog_id` FOREIGN KEY (`prog_id`) REFERENCES `program` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_session_id` FOREIGN KEY (`session_id`) REFERENCES `session` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_student_u_id` FOREIGN KEY (`u_id`) REFERENCES `user` (`u_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_syllabus_id` FOREIGN KEY (`syllabus_id`) REFERENCES `syllabus` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher`
--
ALTER TABLE `teacher`
  ADD CONSTRAINT `fk_dept2_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `teacher_batch`
--
ALTER TABLE `teacher_batch`
  ADD CONSTRAINT `fk_batch2_id` FOREIGN KEY (`batch_id`) REFERENCES `batch` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_dept8_id` FOREIGN KEY (`dept_id`) REFERENCES `dept` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_teacher4_id` FOREIGN KEY (`teacher_id`) REFERENCES `teacher` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
